package com.ashishmhalankar.loginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    EditText edUserName,edPassword;
    Button btnPassword;
    String username="admin", password="admin";

    SharedPreferenceManager sharedPreferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferenceManager=new SharedPreferenceManager(this);

        init();
        checkLogin();
    }

    private void init() {

        edUserName=findViewById(R.id.edUserName);
        edPassword=findViewById(R.id.edPassword);
        btnPassword=findViewById(R.id.btnPassword);

        btnPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()){
                    if (edUserName.getText().toString().equals(username)&&edPassword.getText().toString().equals(password)){
                        Intent intent=new Intent(MainActivity.this,HomeScreen.class);
                        startActivity(intent);
                        finish();
                        sharedPreferenceManager.connectDB();
                        sharedPreferenceManager.setBoolean("isLogin",true);
                        sharedPreferenceManager.closeDB();
                    }
                }
            }
        });

    }

    void checkLogin(){
        sharedPreferenceManager.connectDB();
        if (sharedPreferenceManager.getBoolean("isLogin")){
            Intent intent=new Intent(MainActivity.this,HomeScreen.class);
            startActivity(intent);
            finish();
        }
        sharedPreferenceManager.closeDB();
    }

    Boolean isValid(){
        if (edUserName.getText().toString().isEmpty()){
            edUserName.setError("Enter Username");
            return false;
        }
        if (edPassword.getText().toString().isEmpty()){
            edPassword.setError("Enter Password");
            return false;
        }
        return true;
    }
}
