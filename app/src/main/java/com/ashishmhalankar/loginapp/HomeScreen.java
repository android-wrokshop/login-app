package com.ashishmhalankar.loginapp;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeScreen extends AppCompatActivity {

    Button btnLogout;
    SharedPreferenceManager sharedPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        sharedPreferenceManager=new SharedPreferenceManager(this);

        btnLogout=findViewById(R.id.btnLogout);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                sharedPreferenceManager.connectDB();
                sharedPreferenceManager.setBoolean("isLogin",false);
                sharedPreferenceManager.closeDB();

                Intent intent=new Intent(HomeScreen.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }
}
